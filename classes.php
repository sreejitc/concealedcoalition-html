<?php include 'include/header.php';?>
<?php include 'include/header-parts.php';?> 


<!-- Top Header -->
<div class="inner_banner pt-3 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="h4text color_white m-0">Classes</h1>
                <p class="ptextwithbg text-left color_white">Concealed Carry Training - Local & Online Classes Everywhere</p>
            </div>
        </div>
    </div>
</div>
<div id="sticky-anchor"></div>

<div class="top_breadcrumb py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <a href="#">Classes</a> <i class="fa fa-angle-right" aria-hidden="true"></i> <a href="#">California</a>
            </div>
        </div>
    </div>
</div>
<!-- /Top Header -->


<!-- Zip Code -->
<div class="zip_sections_holder  pb-5">
    <div class="container">
         
            <div class="zip_sections">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-7  pr-0">
                        <div class="zipfleld_div zipfleld_div2">
                            <input type="tel" placeholder="Enter Zip Code" class="zipfleld zipfleld2 search_zip_small form-control" maxlength="5" value="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 "><button class="search_zip search_zip_small">Search</button></div>
                </div>       
            </div>

    </div>
</div>
<!-- /Zip Code -->


<!-- Classes Boxs -->
<div class="classes_box  pb-3">
    <div class="container">
         
            <div class="classes_box_sections">
                <div class="row align-items-end">
                    
                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 16 - SUN, JUL 17<br>
                            9:00 AM - 5:30 PM <br>
                            (2-day course)</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> Manteca, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/BassProShops-Manteca-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> Clovis, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/BestWesternWestCovina-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 16 - SUN, JUL 17<br>
                            9:00 AM - 5:30 PM <br>
                            (2-day course)</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> Manteca, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/BassProShops-Manteca-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 d-flex ">
                        <div class="classes_box_inner">
                            <h2 class="classboxtitleh2">SAT, JUL 23 8:00 AM - 5:00 PM</h2>
                            <p class="classboxlocationsp"><i class="fa-solid fa-location-dot"></i> West Covina, California</p>
                            <a href="javascript:void(0);">
                            <img class="img_fluid" src="images/FairfieldInnMarriott-Clovis-ca.jpg" alt="California" >
                            </a>
                            <div class="classboxlink">
                                <a href="javascript:void(0);" class="btn btn-danger btn-theme font-italic d-block border-0 rounded-0">Register</a>     
                            </div>
                        </div>
                    </div>

 

               
                </div>       
            </div>

    </div>
</div>
<!-- /Classes Boxs -->
   

<?php include 'include/footer.php';?>