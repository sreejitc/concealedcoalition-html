<?php include 'include/header.php';?>
<?php include 'include/header-parts-logo.php';?> 

 
<!-- Top Band -->
<div class="red_band red_band_call text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <p class="redband_txt">
                <strong> 
                    <img src="images/chk-lftarrow.png" alt="Arrow" class="chk-lftarrow" /> Call Today! <a href="tel:800805-2238">(800) 805-2238</a> - 
            Limited Space - <br class="for_mobile">Reserve Your Spot Now! 
            <img src="images/chk-rgtarrow.png" alt="Arrow" class="chk-rgtarrow" /> </strong>
                    </p>
            </div>
        </div>
    </div>
</div>
<!-- /Top Band -->        


<!-- Top Header -->
<div class="inner_banner pt-3 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 class="h4text color_white m-0">California Concealed Carry Permit Training</h1>
                <p class="ptextwithbg color_white">Get Your Name on The List! Space is Extremely Limited.</p>
            </div>
        </div>
    </div>
</div>

 
<!-- /Top Header -->

 


<!-- Classes Boxs -->
<div class="checkout_page checkout_page1  pt-5 pb-4 mb-2">
    <div class="container-lg">
         
           <div class="row">

                <div class="col-lg-5 col-md-6  order-lg-2 order-md-2 checkout_page_right mb-3">
                        <div class="form_border p-4">
                            <h6 class="h6text"><strong>California Concealed Carry Permit Training</strong> </h6>    
                            <img src="images/898889Rancho_Cucamonga_bass_pro.JPG" alt="California" class="img-fluid  fluid100 mt-3">

                            
                            <div class="property_info mt-3">
                                <div class="d-flex">
                                    <div class="px-3">
                                        <p>California Concealed Carry  <br> PermitTraining at Rancho Cucamonga  California Bass Pro Shops </p>

                                    </div>
                                    <div class="px-3 text-right"> <strong>$179.99</strong> </div>                                   
                                </div>
                                <div class="px-3">  
                                    <span><i class="fa-solid fa-calendar-days"></i> Date: July 9, 2022</span>
                                    <span><i class="fa-regular fa-clock"></i> Time: 8:00 AM - 5:00 PM</span> 
                                </div>
                            </div>
                             

                            <div class="text-center d-none d-md-block">
                                <img src="images/ck-seal-new.png" alt="California" class="img-fluid mt-3">
                            </div>


                        </div>
                </div>

                <div class="col-lg-7 col-md-6 order-lg-1 order-md-1  checkout_page_left mb-3">
                        <div class="checkout_form_header d-flex">
                                <div class="form_header_profile mr-3">
                                    <img src="images/step2-frmicon.jpg" alt="Profile" />
                                </div>
                                <div class="form_header_details">
                                    <p class="step_p">Step 1 of 2 <img src="images/frmhdline.png" alt="Line" class="line_p"></p>
                                    <h3 class="info_h3">Enter Your <strong>Contact Information</strong></h3>
                                </div>
                        </div>

                        <div class="floating_labels form_control checkout_page_forms form_border loader_active p-4">

                            <div class="if_loader">
                                <div class="if_loader_inn">
                                    <div class="if_loader_inn_loader">
                                        <div class="if_loader_inn_loader_body">
                                            <div class="spinner-border text-dark" role="status">
                                                <span class="sr-only"></span>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"   class="form-control is-invalid form-control-lg rounded-0  shadow-none" placeholder="First Name" >
                                                <label >First Name</label>  
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>                                 
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"  class="form-control form-control-lg is-invalid rounded-0   shadow-none" placeholder="Last Name" >
                                                <label>Last Name</label>
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-6 pr-2">
                                    
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"   class="form-control form-control-lg is-invalid rounded-0   shadow-none" placeholder="Email"  >
                                                <label >Email</label>
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-6 pl-2">
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"   class="form-control is-invalid form-control-lg rounded-0   shadow-none" placeholder="Phone"  >
                                                <label >Phone</label>
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>   
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"   class="form-control form-control-lg rounded-0   shadow-none"  placeholder="Shipping Address"  >
                                                <label>Shipping Address</label>
                                            </div>
                                        </div>
                                    </div>

                                    

                                    <div class="col-lg-6 pr-2">
                                    
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"  class="form-control form-control-lg is-invalid rounded-0   shadow-none" placeholder="City"  >
                                                <label>City</label>
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-6 pl-2">
                                        <div class="form-label-group in-border">
                                            <div class="form-label position-relative is-invalid">
                                                <input type="text"   class="form-control is-invalid form-control-lg rounded-0   shadow-none" placeholder="Zip Code" >
                                                <label >Zip Code</label>
                                            </div>   
                                            <div class="invalid-feedback">Please fill out this field.</div>   
                                        </div>
                                    </div>

                                    <div class="col-sm-12 px-5 mt-3 button_checkout">
                                        <div class="form-label-group in-border">
                                                <button class="search_zip">Next</button>                                     
                                        </div>                                   
                                    </div>

                                    <div class="col-sm-12  text-center button_checkout">

                                            <div class="check_progress">
                                                    <div class="check_progress_inner"></div>
                                                </div>
                                        
                                        <p class="texthurry   text-center">Hurry! Only <span>a few seats left</span>! </p>
                                        <p class="aggre_text">By clicking Next I provide my signature expressly consenting contact from Concealed Coalition and consent to 
                                                    receive automated and/or prerecorded telemarketing calls/texts relating to firearm(s) &amp;
                                                    training products and services. I understand that consent is not a condition of purchase. 
                                                    To agree without providing consent to be contacted by these means please call us at 
                                                    (800) 805-2238.</p>
                                                <div class="text-center mt-4">    
                                                        <img src="images/logos.png" alt="Logos" class="logos">
                                                </div>
                                    </div>

                                </div>
                            </form>

                            

                        </div>    
                </div>

                
            
           </div>

    </div>
</div>
<!-- /Classes Boxs -->





<?php include 'include/footer-without-sticky.php';?>