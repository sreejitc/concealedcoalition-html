<?php include 'include/header.php';?>
<?php include 'include/header-parts.php';?>
 
<!-- Top Band -->
<div class="red_band">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <p class="redband_txt"><span>ATTENTION GUN OWNERS & FUTURE GUN OWNERS:</span> GET CONCEALED CARRY CERTIFIED BEFORE IT'S TOO LATE</p>
            </div>
        </div>
    </div>
</div>
<div id="sticky-anchor"></div>
<!-- /Top Band -->
 
<!-- Banner -->
<div class="banner control_focus_white">           
    <div class="banner_inner" >

         <div class="embed-responsive embed-responsive-16by9" style="background-image: url('images/video3.jpg');">
           <video id="l_video" loop autoplay muted playsinline  preload="auto">
              <source src="https://concealed.s3-us-west-2.amazonaws.com/Web-banner-2.mp4" type="video/mp4">
           </video>
        </div>
 

            <div class="video_overlay for_desktop_overlay">
                <div class="video_overlay_mid">
                    <div class="video_overlay_inn">
                    
                    <div class="container">    
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>CONCEALED CARRY TRAINING</h1>
                                <h2>LOCAL & ONLINE CLASSES EVERYWHERE</h2>
                                
                                <div class="zip_sections banner_zip mt-4">
                                    <div class="col-lg-12">
                                        
                                        <div class="row justify-content-center">  
                                            <div class="col-lg-4 col-md-6 pr-0">
                                            <div class="zipfleld_div">
                                                <input type="tel" placeholder="Enter Zip Code" class="zipfleld form-control" maxlength="5" value="">
                                            </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 pr-0"><button class="search_zip">Get Certified  <span class="span1"></span></button></div>
                                        </div>

                                    </div>       
                                </div>
                            </div>
                        </div> 
                    </div>        





                    </div>
                </div>
            </div>

       

    <div class="clearfix"></div>
    </div>
</div>
<!-- /Banner -->

<!-- Mobile Zip -->
<div class="video_overlay control_focus_white for_mobile_overlay">
    <div class="video_overlay_mid">
        <div class="video_overlay_inn">
        
        <div class="container">    
            <div class="row">
                <div class="col-lg-12">
                    <h1>CONCEALED CARRY TRAINING</h1>
                    <h2>LOCAL & ONLINE CLASSES EVERYWHERE</h2>
                    
                    <div class="zip_sections banner_zip mt-4">
                        <div class="col-lg-12">
                            
                            <div class="row justify-content-center">  
                                <div class="col-lg-4 col-md-6 pr-0">
                                    <div class="zipfleld_div">
                                        <input type="tel" placeholder="Enter Zip Code" class="zipfleld form-control" maxlength="5" value="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 pr-0"><button class="search_zip">Get Certified  <span class="span1"></span></button></div>
                            </div>

                        </div>       
                    </div>
                </div>
            </div> 
        </div>        





        </div>
    </div>
</div>
<!-- /Mobile Zip -->

 
 

<div class="clearfix"></div>
<!-- icon section -->
<div class="icon_list_sections py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <ul class="icon_list">
                <li>
                    <img src="images/over-1000000-trained.png" loading="lazy" alt="Over 1,000,000 Trained">
                    <h2 class="mb-0  mt-2"> Over 1,000,000 <br>Trained </h2>
                </li>
                <li>
                    <img src="images/multistate-certified-instructors.png" loading="lazy" alt="Multi-State Certified Instructors">
                    <h2  class="mb-0 mt-2">Multi-State Certified <br>Instructors</h2>
                </li>
                <li>
                    <img src="images/online-and-localtraining-classes.png" loading="lazy" alt="Online &amp; Local Training Classes">
                    <h2  class="mb-0  mt-2">Online &amp; Local Training <br>Classes</h2>
                </li>
                <li>
                    <img src="images/home-study-ciriculumn.png" loading="lazy" alt="#1 Home Study Curriculum">
                    <h2  class="mb-0  mt-2">#1 Home Study <br>Curriculum</h2>
                </li>
            </ul>

            </div>
        </div>
    </div>
</div>
<!-- /icon section -->


<!-- Box section -->
<div class="box_list_sections box_flex">
    <div class="container">
       
        <div class="row">
            <div class="col-lg-12">

                <h2 class="h2text mb-4">Become A Responsibly Armed <br> American Citizen</h2>
                <p class="ptextwithbg">At Concealed Coalition, our purpose is to change the way people feel about firearms.
                     Every day we open minds through next-gen digital learning and extinguish fear through hands-on education experiences. Concealed Coalition is leading the industry with a direct passion for firearms. We have become the nationwide online authority for concealed carry permit training. Concealed Coalition has changed the game in "on the ground" localized CCW certification by delivering an emotional, entertaining, and informative hands-on concealed carry training experience. We have a network of first-class certified trainers, classroom location partnerships with national brands and an extensive gun range alliance. We exist to inspire hope, extinguish fear, 
                    and bring help to people searching for solutions in the real world. Join us.</p>
 
            </div>
            <div class="clearfix"></div>
        </div>    

        <div class="row box_for3">
            <div class="col-md-4">
                <div class="box_list mt-5">
                    <img loading="lazy" alt="Education" src="images/Education-new.jpg" width="370" height="240">
                    <div class="box_list_content py-4">
                        <h3  class="h3text">Education</h3>
                        <p class="ptextwithbg">Online video courses on <br>gun safety practices.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box_list  mt-5">
                    <img loading="lazy" alt="Training" src="images/s1img2.jpg" width="370" height="240">
                    <div class="box_list_content py-4">
                        <h3  class="h3text">Training</h3>
                        <p class="ptextwithbg">Firearm training to help you defend<br> yourself & your family.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box_list  mt-5">
                    <img loading="lazy" alt="Certification" src="images/s1img3.jpg" width="370" height="240">
                    <div class="box_list_content py-4">
                        <h3  class="h3text">Certification</h3>
                        <p class="ptextwithbg">Pass the test online and download your<br> certificate instantly.</p>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            </div> 

        </div>


     
</div>
<!-- /Box section -->


<!-- trained sections -->
<div class="trained_sections control_focus_white">
    <div class="container">
        <div class="row  align-items-center">
            
            <div class="col-lg-6 order-lg-2 trained_sections_img">
                
                <div class="row">

                    <div class="col-12 mb-3 for_tab">
                        <h4 class="h4text">Get Trained By <br class="br_show_mobile">Certified<br> Instructors </h4>
                    </div>

                    <div class="col-4">
                        <img loading="lazy" alt="Trained 1" src="images/thumb1-band12.jpg" width="215" height="263" class="img-fluid">
                    </div>
                    <div class="col-4">
                        <img loading="lazy" alt="Trained 2" src="images/thumb2-band12.jpg" width="215" height="263" class="img-fluid">
                    </div>
                    <div class="col-4">
                        <img loading="lazy" alt="Trained 3" src="images/thumb3-band12.jpg" width="215" height="263" class="img-fluid">
                    </div>  

                    <div class="clearfix"></div>
                </div>
            
            </div> 

            <div class="col-lg-6 text-left order-lg-1">
                        <h4 class="h4text for_desktop">Get Trained By Certified<br> Instructors </h4>
                        
                        <h5 class="h5text">Enter your ZIP code & we will help you get a <br>
                        concealed carry permit valid in your state!</h5>
                        <div class="zip_sections trained_zip">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="zipfleld_div">
                                        <input type="tel" placeholder="Enter Zip Code" class="zipfleld form-control" maxlength="5" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-0"><button class="search_zip">Get Certified  <span class="span1"></span></button></div>
                            </div>       
                        </div>
            </div> 


            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- /trained sections -->


<!-- State Classes -->
<div class="state_classes_sections py-5">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-12">
                <h2 class="h2text mb-0">Concealed Carry <br class="br_show_mobile">State Classes</h2>
                <h6 class="h6text"><strong>Upcoming Classes</strong> in United States</h6>
            </div>
 
        </div>

            <div class="zip_sections mt-3">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6  pr-0">
                        <div class="zipfleld_div zipfleld_div2">
                            <input type="tel" placeholder="Enter Zip Code" class="zipfleld zipfleld2 search_zip_small form-control" maxlength="5" value="">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 "><button class="search_zip search_zip_small">Search</button></div>
                </div>       
            </div>

    </div>
</div>
<!-- /State Classes -->

<!-- State Classes -->
<div class="steps_3 py-5 mt-5 mb-3">
    <div class="container">
 
        <div class="row">
             
            <div class="col-lg-12">
                <h2 class="h2text mb-3">3 Steps <br>To Your CCW Permit</h2>
                <p class="ptextwithbg">It has never been easier in your state to get your Concealed Carry Permit!</p>
            </div>

            <div class="clearfix"></div>
            <div class="owl-carousel owl-theme owl_arrow" id="owl001">

                <div class="item">
                <div class="steps_3_box mt-5 px-5 text-center">
                    <div class="steps_3_box_image">
                        <picture class="rounded-circle">
                            <source type="image/webp" width="293" height="270" srcset="images/Get-Certified-Icon.webp">
                            <source type="image/jpg" width="293" height="270" srcset="images/Get-Certified-Icon.jpg">
                            <img class="rounded-circle img-fluid" loading="lazy" alt="Get Certified" src="images/Get-Certified-Icon.jpg" width="293" height="270">
                        </picture>
                    </div>
                    <div class="box_list_content pt-4 ">
                        <p class="ptextwithbg ptextwithbgfont mt-3">Step 1</p>
                        <h3 class="h3text mt-2">Get Certified</h3>
                        <p class="ptextwithbg">Take Our Course and  Complete Range Training if required</p>
                    </div>
                </div>
                </div>
                <div class="item">
                <div class="steps_3_box steps_3_box_mid  mt-5  px-5 text-center">
                    <div class="steps_3_box_image">
                        <picture class="rounded-circle">
                            <source type="image/webp" width="293" height="270" srcset="images/s3img2.webp">
                            <source type="image/jpg" width="293" height="270" srcset="images/s3img2.png">
                            <img class="rounded-circle img-fluid" loading="lazy" alt="Get Certified" src="images/s3img2.png" width="293" height="270">
                        </picture>
                    </div>
                    <div class="box_list_content pt-4">
                        <p class="ptextwithbg ptextwithbgfont mt-3">Step 2</p>
                        <h3 class="h3text mt-2">Get Certified</h3>
                        <p class="ptextwithbg">Submit Your Certification and Application</p>
                    </div>
                </div>
                </div>
                <div class="item">
                <div class="steps_3_box mt-5  px-5 text-center">
                    <div class="steps_3_box_image">
                        <picture class="rounded-circle">
                            <source type="image/webp" width="293" height="270" srcset="images/Certi.webp">
                            <source type="image/jpg" width="293" height="270" srcset="images/Certi.png">
                            <img class="rounded-circle img-fluid" loading="lazy" alt="Get Certified" src="images/Certi.png" width="293" height="270">
                        </picture>
                    </div>
                    <div class="box_list_content pt-4">
                        <p class="ptextwithbg ptextwithbgfont mt-3">Step 3</p>
                        <h3 class="h3text mt-2">Get Certified</h3>
                        <p class="ptextwithbg">Get Your CCW Permit</p>
                    </div>
                </div>
                </div>

                </div>


            <div class="clearfix"></div>
 
            
        </div>

            <div class="clearfix"></div>    

            <div class="zip_sections banner_zip mt-5 for_desktop">
                <div class="col-md-10 offset-md-1">
                    
                    <div class="row justify-content-center">  
                        <div class="col-lg-4 col-md-6">
                            <div class="zipfleld_div zipfleld_div2">
                                <input type="tel" placeholder="Enter Zip Code" class="zipfleld zipfleld2 form-control" maxlength="5" value="">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 pl-0"><button class="search_zip">Get Certified  <span class="span1"></span></button></div>
                    </div>

                </div>       
            </div>

            

    </div>
</div>
<!-- /State Classes -->

<!-- trained 2 sections -->
<div class="trained_sections2 control_focus_white">
    <div class="container">
        <div class="row  align-items-center">
           
            <div class="col-lg-8 text-left">
                        <h4 class="h4text">Get Trained By <br class="br_show_mobile">Certified  Instructors </h4>

                        <div class="for_tab pt-3 pb-4">
                        <img class="img-fluid" loading="lazy" alt="" src="images/get-trained-banner-mob.jpg" width="767" height="337">
                        </div>
                        
                        <h5 class="h5text">Enter your ZIP code & we will help you get a <br>
                        concealed carry permit valid in your state!</h5>
                        <div class="zip_sections trained2_zip mt-4">
                            <div class="row justify-content-center">
                                <div class="col-md-6 pr-0">
                                    <div class="zipfleld_div">
                                        <input type="tel" placeholder="Enter Zip Code" class="zipfleld form-control" maxlength="5" value="">
                                    </div>
                                </div>
                                <div class="col-md-6 pr-0"><button class="search_zip">Get Certified  <span class="span1"></span></button></div>
                            </div>       
                        </div>
            </div> 
 

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- /trained sections -->


<!-- Testimonials -->
<div class="client_testimonials box_flex">
    <div class="container">

        <div class="row ">
            <div class="col-lg-12">
                <h2 class="h2text mb-3">Concealed Carry  <br>Client Testimonials</h2>
            </div>               
        </div>
        
        <div class="client_top_box">
            <div class="row box_for3">
 
                <div class="col-md-4">
                    <div class="client_boxs mt-4">
                        <picture>
                        <source type="image/webp" width="370" height="255" srcset="images/s4img1.webp">
                        <source type="image/png" width="370" height="255" srcset="images/s4img1.jpg">
                        <img loading="lazy" alt="SCOTT B" src="images/s4img1.jpg" width="370" height="255">
                        </picture>
                       
                        <div class="client_box_content">
                            <div class="s4starbx py-3"><img src="images/s4stars.jpg" loading="lazy" alt="Star" class="s4stars"></div>
                            <p class="ptextwithbg px-4">"It worked for me! Glad I found Concealed Coalition, the online class was great! I was able to book a live fire training the same week...</p>
                            <p class="ptextwithbg ptextwithst mt-2"><strong>- SCOTT B.</strong></p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 hide_mobile">
                    <div class="client_boxs mt-4">
                        <picture>
                        <source type="image/webp" width="370" height="255" srcset="images/testimonial-2-new.webp">
                        <source type="image/png" width="370" height="255" srcset="images/testimonial-2-new.jpg">
                        <img loading="lazy" alt="Ally G" src="images/testimonial-2-new.jpg" width="370" height="255">
                        </picture>
                       
                        <div class="client_box_content">
                            <div class="s4starbx py-3"><img src="images/s4stars.jpg" loading="lazy" alt="Star" class="s4stars"></div>
                            <p class="ptextwithbg px-4">"Awesome class! Very fun, interactive, & educational! Highly recommend!."</p>
                            <p class="ptextwithbg ptextwithst mt-2"><strong>- Ally G.</strong></p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4 hide_mobile">
                    <div class="client_boxs mt-4">
                        <picture>
                        <source type="image/webp" width="370" height="255" srcset="images/testimonial-3-new.webp">
                        <source type="image/png" width="370" height="255" srcset="images/testimonial-3-new.jpg">
                        <img loading="lazy" alt="Aaron S" src="images/testimonial-3-new.jpg" width="370" height="255">
                        </picture>
                       
                        <div class="client_box_content">
                            <div class="s4starbx py-3"><img src="images/s4stars.jpg" loading="lazy" alt="Star" class="s4stars"></div>
                            <p class="ptextwithbg px-4">"Very knowledgeable and friendly instructors! The class was very 
                                enjoyable and informative. Thank you to all of our instructors!"</p>
                            <p class="ptextwithbg ptextwithst mt-2"><strong>- Aaron S.</strong></p>
                        </div>

                    </div>
                </div>

                
            </div>
        </div>


        <div class="client_top_box_list  mt-4">
            <div class="row">
 
                <div class="col-md-12 ">
                    
                    <div class="d-flex align-items-top client_boxs_list py-4">
                            <div class="position_relative">
                                <div class="s4rv_namebox" style="background-image: url('images/Review-HS-COLT-M.jpg');">
                                    <img src="images/name-tick.png" loading="lazy" alt="Valid">
                                </div>
                            </div>
                       
                        <div class="client_box_content_list pl-5">
                            <img src="images/s4stars.jpg" loading="lazy" alt="Star">
                            <p class="ptextwithbg text-left pt-2"> "Instructors are friendly and knowledgeable and willing to answer tough questions. they spend a lot of time researching laws and sharing valuable information with the class."</p>
                            <p class="ptextwithbg text-left ptextwithst mt-2"><strong>- Colt M.</strong></p>
                        </div>

                    </div>

                    <div class="d-flex align-items-top client_boxs_list py-4">
                            <div class="position_relative">
                                <div class="s4rv_namebox" style="background-image: url('images/Review-HS-Marni-L.jpg');">
                                    <img src="images/name-tick.png" loading="lazy" alt="Valid">
                                </div>
                            </div>
                       
                        <div class="client_box_content_list pl-5">
                            <img src="images/s4stars.jpg" loading="lazy" alt="Star">
                            <p class="ptextwithbg text-left pt-2">"Very informative class. All of the staff were very friendly and helpful. Highly recommend!"</p>
                            <p class="ptextwithbg text-left ptextwithst mt-2"><strong>- Marni L.</strong></p>
                        </div>

                    </div>

                    <div class="d-flex align-items-top client_boxs_list border-0 py-4">
                            <div class="position_relative">
                                <div class="s4rv_namebox" style="background-image: url('images/Review-HS-DREA-D.jpg');">
                                    <img src="images/name-tick.png" loading="lazy" alt="Valid">
                                </div>
                            </div>
                       
                        <div class="client_box_content_list  pl-5">
                            <img src="images/s4stars.jpg" loading="lazy" alt="Star">
                            <p class="ptextwithbg text-left pt-2">"Was a great class. I'm a first-time gun owner and I learned a lot. Appreciate the class!"</p>
                            <p class="ptextwithbg text-left ptextwithst mt-2"><strong>- Drea D.</strong></p>
                        </div>

                    </div>

 

                </div>
 
                
            </div>
        </div>
 

    </div>
</div>

<!-- /Testimonials -->



 


<?php include 'include/footer.php';?>