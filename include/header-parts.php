  
<!-- Header -->
<header class="top_header" id="sticky">

   <div class="container">

      <div class="row">
        <div class="col-lg-12">
        
          <nav class="navbar navbar-expand-lg ">
            

             <a class="navbar-brand" href="index.php"> <img loading="lazy" alt="Concealedcoalition" class="logo" src="images/logo.png" width="250" height="48"> </a>
              <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
              <div class="navbar-collapse collapse desktop_nav" id="navbarNavDropdown" style="">
                <ul class="navbar-nav ml-md-auto">
                  <li class="nav-item"><a href="classes.php" class="nav-link">Classes</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Trainers</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Locations</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Gun Laws</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Reciprocity Maps</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Gift Cards</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Blog</a></li>
                  <li class="nav-item"><a href="#" class="nav-link">Join Now</a></li>
                  <li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#myModal">Get Certified</a></li>
               </ul>  
              </div>
              <a href="javascript:void(0);" class="hamburger_menu hamburger_menu_innav" ><span></span><span></span><span></span></a> 
          </nav>
        </div>
        <div class="clearfix"></div>
      </div>


         

      <div class="clearfix"></div>
   </div>


   <div class="membrs_login_head">
      <a href="#" class="membrs_login_head_text"><i class="fa-solid fa-user"></i> Members Login </a>
   </div>
    
</header>

<div class="menu_holder" >
   <ul>
   <li><a href="classes.php" class="menu-link">Classes</a></li>
   <li><a href="#"  class="menu-link">Trainers</a> </li>
   <li><a href="#"  class="menu-link">Locations</a></li>
   <li><a href="#"  class="menu-link">Gun Laws</a></li>
   <li><a href="#"  class="menu-link">Reciprocity Maps</a></li>
   <li><a href="#"  class="menu-link">Gift Cards</a></li>
   <li><a href="#"  class="menu-link" >Join Now</a></li>
   <li><a href="#"  class="menu-link">Blog</a></li>
   <li><a href="#"  class="menu-link">Members Login</a> </li>
   <li><a href="#" data-toggle="modal" data-target="#myModal2">Get Certified</a> </li>
   </ul>
</div>

<!-- /Header -->
      
