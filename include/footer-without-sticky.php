

<!-- Footer Nav -->
<div class="getin_touch py-5">
    <div class="container">
        <div class="row align-items-center">
            
            <div class="col-lg-12 text-center">
                <h2 class="mb-4">Get In Touch</h2>
            </div>

            <div class="col-md-6 contact_us_footer text-center">
                <h3 class="mt-3 mb-4">Contact Us</h3>    
                <p>We are located at <br>
                848 North Rainbow Blvd, #508<br>
                Las Vegas, Nevada 89107</p>

                <p><a href="tel:800805-2238">(800) 805-2238</a> </p>
            
            </div>

            <div class="col-md-6 text-center">
                    <img src="images/s5icon3.png" alt="Connect With Us" >
                    <h3  class="mt-3 mb-4">Connect With Us</h3>    
                    <p>
                        <a href="tel:800805-2238">(800) 805-2238</a> <br>
                        <a href="mailto:info@concealedcoalition.com">info@concealedcoalition.com</a>
                    </p>
                    <ul class="mt-4">
                        <li><a href="https://www.instagram.com/ConcealedCoalition/" target="_blank"><i class="fa-brands fa-instagram"></i></a></li>
                        <li><a href="https://www.facebook.com/ConcealedCoalition" target="_blank"><i class="fa-brands fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/TheCCCoalition" target="_blank"><i class="fa-brands fa-twitter"></i></a></li>
                    </ul>
            </div>


        </div>
    </div>
</div>
<!-- /Footer Nav -->

<!-- Footer Nav -->
<div class="footer_nav py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <ul class="footernav_list">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Classes</a></li>
                    <li><a href="#">Trainers</a></li>
                    <li><a href="#">Locations</a></li>
                    <li><a href="#">Gun Laws</a></li>
                    <li><a href="#">Reciprocity Maps</a></li>
                    <li><a href="#" >Gift Cards</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#" >Join Now</a></li>
                    <li><a href="#">Store</a></li>
                    <li><a href="#">Members Login</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /Footer Nav -->

<!-- Footer  -->
<footer class="footer_sections py-5">
    <div class="container">
        <div class="row  align-items-center">
            <div class="col-lg-6 col-md-4 text-left">
                <picture class="ftrlogo">
                    <source type="image/webp" width="250" height="48" srcset="images/logo.png">
                    <source type="image/png" width="250" height="48" srcset="images/logo.png">
                    <img loading="lazy" alt="Concealedcoalition" src="images/logo.png" width="250" height="48">
                </picture>

                <p class="mt-3 for_desktop">All Rights Reserved <script>var d = new Date();document.write(d.getFullYear());</script>  Copyright 
               Concealed Coalition, Inc.<br><span>v9.1.1 </span> </p>

            </div>
            <div class="col-lg-6 col-md-8 text-right">
            <p class=" footer_sec_nav">
                <a href="#">Site Map</a> &nbsp;|&nbsp; 
                <a href="#">Privacy Policy</a> &nbsp;|&nbsp;
                <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; 
                <a href="#">Refund Policy</a>
                <br><a href="#">Do not sell my info - CA residents only. </a>
            </p>
            </div>

            <div class="col-md-12 text-left for_tab">    
                <p class="mt-3">All Rights Reserved <script>var d = new Date();document.write(d.getFullYear());</script>  Copyright 
               Concealed Coalition, Inc.<br><span>v9.1.1 </span> </p>
            </div>


        </div>
    </div>
</footer>
<!-- /Footer  -->


<!-- Js -->
<script src="js/jquery.min.js" ></script> 
<script src="js/bootstrap.bundle.min.js"></script> 
<script src="js/owl.carousel.js"></script>

<script>
	
	$('#owl001').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        767:{
            items:3,
					nav:true
				},
        1000:{
            items:3,
            nav:true,
            loop:false
        }
			}
		})
	 
		
		
	</script>
 

<script>

$(document).ready(function(){

    function slideMenu() {
      var activeState = $(".menu_holder").hasClass("active");
      $(".menu_holder").animate({left: activeState ? "0%" : "-100%"}, 400);
    }
    $(".hamburger_menu").click(function(event) {
      event.stopPropagation();
      $(".hamburger_menu").toggleClass("open");
      $(".menu_holder").toggleClass("active");
      slideMenu();
      $("body").toggleClass("overflow-hidden");
    });
   
});



</script>  

 
<script>
    if(navigator.userAgent.indexOf('Mac') > 0)
    $('body').addClass('mac-os');
</script>

 

      
   </body>
</html>
